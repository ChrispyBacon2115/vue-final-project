import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {RentalDataAccess as roda} from "@/api"
import {CustomerDataAccess as cd} from "@/api"
import {RoleDataAccess as rda} from "@/api"

Vue.config.productionTip = false

new Vue({
  data(){
    return{
        customers:[],
        rentals:[],
        userRoles:[],
        currentUser:"",
        currentUserRoleId:0,
        currentUserId:0
    }
  },
  mounted(){
    roda.getAllRentals()
    .then(response => {
        this.rentals = response.data
    })
    .catch(error => console.log(error)),
    cd.getAllCustomers()
    .then(response => {
      this.customers = response.data
    })
    .catch(error => console.log(error)),
    rda.getAllRoles()
    .then(response => {
        this.userRoles = response.data
    })
    .catch(error => console.log(error))
  },
  router,
  render: h => h(App)
}).$mount('#app')
